# REST API

source(s): [restfulapi.net](https://restfulapi.net/), [Wikipedia](https://en.wikipedia.org/wiki/Representational_state_transfer), 

REST is an acronym for REpresentational State Transfer. It is a software architectural style with a set of constraints to be used for creating Web services.

There are six guiding constraints that have to be satisfied for an interface to be considered RESTful:

1. Client-server
2. Stateless
3. Cacheable
4. Uniform interface
5. Layered system
6. Code on demand (optional)
