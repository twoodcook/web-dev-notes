<img src="images/css-flexbox.png" width="700px">  

source(s): [w3schools.com](https://www.w3schools.com/css/css3_flexbox.asp), [w3.org](https://www.w3.org/TR/css-flexbox-1/#alignment),  

[CODE EXAMPLE 01](https://www.w3schools.com/code/tryit.asp?filename=G77J4UCKS8BU) - Three column, justified center, wrap  
[CODE EXAMPLE 02](https://www.w3schools.com/code/tryit.asp?filename=G77JF14DT1SL) - Center content vertically and horizontally within container


# FLEX CONTAINER - Properties for the Parent

## display
This defines a flex container; inline or block depending on the given value. It enables a flex context for all its direct children.
```css
Display: flex | inline-flex;
```

## flex-direction
This establishes the main-axis, thus defining the direction flex items are placed in the flex container. Flexbox is (aside from optional wrapping) a single-direction layout concept. Think of flex items as primarily laying out either in horizontal rows or vertical columns.
```css
flex-direction: row | row-reverse | column | column-reverse;
```

## flex-wrap
By default, flex items will all try to fit onto one line. You can change that and allow the items to wrap as needed with this property.
```css
flex-wrap: nowrap | wrap | wrap-reverse;
```

## flex-flow
This is a shorthand for the flex-direction and flex-wrap properties, which together define the flex container's main and cross axes. The default value is row nowrap.
```css
flex-flow: <‘flex-direction’> || <‘flex-wrap’>;
```

## justify-content
This defines the alignment along the main axis. It helps distribute extra free space left over when either all the flex items on a line are inflexible, or are flexible but have reached their maximum size. It also exerts some control over the alignment of items when they overflow the line.  

*Note: space-between is often a good choice. It places the first item on a line along the left margin, and the last item on the line against the right margin.*

```css
justify-content: flex-start | flex-end | center | space-between | space-around | space-evenly;
```

<img src="/images/flexbox-justify-content.png" width="300px">

## align-items
This defines the default behavior for how flex items are laid out along the cross axis on the current line. Think of it as the justify-content version for the cross-axis (perpendicular to the main-axis). [EXAMPLE](https://css-tricks.com/wp-content/uploads/2018/10/align-items.svg)  

*Note: align-items applies to the flex container (parent), you can also use [align-self](#align-self), which applies to flex-items (children)

```css
align-items: stretch | flex-start | flex-end | center | baseline;
```  

<img src="/images/flexbox-align-items.png" width="300px">  

## align-content
This aligns a flex container's lines within when there is extra space in the cross-axis, similar to how justify-content aligns individual items within the main-axis. Note: this property has no effect when there is only one line of flex items. EXAMPLE
```css
align-content: flex-start | flex-end | center | space-between | space-around | stretch;
```

<img src="/images/flexbox-align-content.png" width="300px">  

---

# FLEX ITEMS - Properties for the Children

## order
By default, flex items are laid out in the source order. However, the order property controls the order in which they appear in the flex container.
```css
order: <integer>; /* default is 0 */
```

## flex-grow
This defines the ability for a flex item to grow if necessary. It accepts a unitless value that serves as a proportion. It dictates what amount of the available space inside the flex container the item should take up. If all items have flex-grow set to 1, the remaining space in the container will be distributed equally to all children. If one of the children has a value of 2, the remaining space would take up twice as much space as the others (or it will try to, at least). [EXAMPLE](https://css-tricks.com/wp-content/uploads/2018/10/flex-grow.svg)
```css
flex-grow: <number>; /* default 0 */
```

## flex-shrink
This defines the ability for a flex item to shrink if necessary.
```css
flex-shrink: <number>; /* default 1 */
```

## flex-basis
This defines the default size of an element before the remaining space is distributed. It can be a length (e.g. 20%, 5rem, etc.) or a keyword. The auto keyword means "look at my width or height property" (which was temporarily done by the main-size keyword until deprecated). The content keyword means "size it based on the item's content" - this keyword isn't well supported yet, so it's hard to test and harder to know what its brethren max-content, min-content, and fit-content do. If set to 0, the extra space around content isn't factored in. If set to auto, the extra space is distributed based on its flex-grow value. [EXAMPLE](https://www.w3.org/TR/css-flexbox-1/images/rel-vs-abs-flex.svg)
```css
flex-basis: <length> | auto; /* default auto */
```

## flex
This is the shorthand for flex-grow, flex-shrink and flex-basis combined. The second and third parameters (flex-shrink and flex-basis) are optional. Default is 0 1 auto. It is recommended that you use this shorthand property rather than set the individual properties. The short hand sets the other values intelligently.
```css
flex: none | [ <'flex-grow'> <'flex-shrink'>? || <'flex-basis'> ]
```

## align-self
This allows the default alignment (or the one specified by align-items) to be overridden for individual flex items. Please see the align-items explanation to understand the available values. Note that float, clear and vertical-align have no effect on a flex item. [EXAMPLE](https://css-tricks.com/wp-content/uploads/2018/10/align-self.svg)
```css
align-self: auto | flex-start | flex-end | center | baseline | stretch;
```
