
# Using Data Attributes

source(s): [developer.mozilla.org](https://developer.mozilla.org/en-US/docs/Learn/HTML/Howto/Use_data_attributes),

## Description & Syntax
`data-*` attributes in HTML5 allow you to store extra information in standard HTML elements.

Any attribute on an HTML element whose attribute name starts with `data-` is a data attribute. This extra information doesn't have any visual representation, but can be used to manipulate the document.

Example:

```html
<article
  id="electric-cars"
  data-columns="3"
  data-index-number="12314"
  data-parent="cars">
...
</article>
```

## JavaScript Access

You could use `getAttribute()` with their full HTML name to read them, but the standard defines a simpler way: a `DOMStringMap` you can read out via a `dataset` property.

To get a data attribute through the dataset object, get the property by the part of the attribute name after data- (note that dashes are converted to camelCase).

Each property is a string and can be read and written. In the below case setting `article.dataset.columns = 5` would change that attribute to "5".

```html
const article = document.querySelector('#electric-cars');
 
article.dataset.columns // "3"
article.dataset.indexNumber // "12314"
article.dataset.parent // "cars"
```

## CSS Access

Since the data attributes are plain HTML attributes, you can even access them from CSS. For example, to show the parent data on the article you can use generated content in CSS with the `attr()` function:

```css
article::before {
  content: attr(data-parent);
}
```

You can also use the attribute selectors in CSS to change styles according to the data:

```css
article[data-columns='3'] {
  width: 400px;
}
article[data-columns='4'] {
  width: 600px;
}
```

## Example Using JavaScript

```html
<!DOCTYPE html> 
<html> 
<head> 
    <script> 
        function showDetails(book) { 
            var bookauthor = book.getAttribute("data-book-author"); 
            alert(book.innerHTML + " is written by "  
                                    + bookauthor + "."); 
        } 
    </script> 
</head> 
  
<body> 
    <h1>Books</h1> 
    <p>Click on the book name to know author's name :</p> 
      
    <ul> 
        <li onclick="showDetails(this)" id="gitanjali" 
                data-book-author="Rabindra Nath Tagore"> 
            Gitanjali 
        </li> 
          
        <li onclick="showDetails(this)" id="conquest_of_self" 
                data-book-author="Mahatma Gandhi"> 
            Conquest of Self 
        </li>  
         
        <li onclick="showDetails(this)" id="broken_wings" 
                data-book-author="Sarojini Naidu"> 
            Broken Wings 
        </li>  
    </ul> 
</body> 
</html>                     
```

RESULT: When we click on the book, we can see the name of the author in a separate dialogue box.
