# Web Development Notes

This is a collection of my own personal notes on web development (specifically, front end web dev). I am creating notes as I learn about new topics, or review familiar ones. Feel free to use these on your own learning path. Click the links below to visit the note file (.md) you wish to review. *Please note: portions of these notes are pulled directly from other sources. Sources are listed at the top of each note file.*

- [Boostrap](/bootstrap.md)
- [CSS Flexbox](/css-flexbox.md)
- [CSS Media Queries](/css-media-queries.md)
- [CSS Position](/css-position.md)
- [CSS rem Units](/css-rem.md)
- [Dark Mode UI](/dark-mode.md)
- [Data Attribute](/data-attribute.md)
- [File Transfer Protocol (FTP)](/ftp.md)
- [JavaScript DOM](/javascript-dom.md)
- [Laws of User Experience](laws-of-ux.md)
- [REST API](rest-api.md)
- [Wordpress](wordpress.md)
