# Media Queries

source(s): [developer.mozilla.org](https://developer.mozilla.org/en-US/docs/Web/CSS/At-rule)

## CSS At-Rules

`@media` is considered an "at-rule" in CSS. More specifically, it is considered a "conditional group rule" along with `@supports`. This means that if a certain criteria is met, the "conditional group rule" (`@media`) will apply.

[developer.mozilla.org](https://developer.mozilla.org/en-US/docs/Web/CSS/At-rule) describes the `@media` at-rule this way:
>"`@media` — A conditional group rule that will apply its content if the device meets the criteria of the condition defined using a media query."

## Syntax

The `@media` at-rule may be placed at the top level of your code or nested inside any other conditional group at-rule.

### Single Condition:
```css
@media only screen and (min-width: 320px) {
  body { line-height: 1.2; }
}
```

### Multiple Conditions:
```css
@media only screen 
  and (min-width: 320px) 
  and (max-width: 480px)
  and (resolution: 150dpi) {
    body { line-height: 1.4; }
}
```

## Conditions / Logic

Essentially, `@media` queries are conditional statements. You can use several operators you can use to set up these conditions:

### And
```css
@media (min-width: 600px) and (max-width: 800px) {
  html { background: red; }
}
```

### Or
```css
@media (max-width: 600px), (min-width: 800px) {
  html { background: red; }
}
```

### Not
```css
@media not all and (max-width: 600px) {
  html { background: red; }
}
```

*Note: source order counts! queries that are introduced later in the source code will overwrite earlier ones in the case of a conflict.*
