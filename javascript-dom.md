# JavaScript DOM Traversal

source(s): [w3schools.com](https://www.w3schools.com/js/js_htmldom_document.asp),

Replacing jQuery with JavaScript can be painful considering how easy jQuery seems to make things. Here are some tips on working with the DOM in pure vanilla JS.

## Quick DOM Facts

- HTML DOM methods are actions you can perform (on HTML Elements).
- HTML DOM properties are values (of HTML Elements) that you can set or change.
- The HTML DOM can be accessed with JavaScript (and with other programming languages).
- In the DOM, all HTML elements are defined as objects.
- The programming interface is the properties and methods of each object.
- A property is a value that you can get or set (like changing the content of an HTML element).
- A method is an action you can do (like add or deleting an HTML element).

## getElementById Method

the most common way to access an HTML element is by it's id.

## innerHTML Property

The easiest way to get and/or replace the content of an HTML element is by using the innerHTML property. (can even change `<html>` and `<body>`)

## The Document Object

The HTML DOM document object is the owner of all other objects in your web page. The document object represents your web page. If you want to access an element in the page, you always start by accessing the document object.

### Finding HTML Elements

| Method | Description |
| :--- | :---- |
| document.getElementById(id) | Find elements by id |
| document.getElementsByClassName(name) | Find elements by class |
| document.getElementsByTagName(name) | Find elements by tag |

### Changing HTML Elements

| Property | Description |
| :--- | :--- |
| element.innerHTML =  new html content | Change the inner HTML of the element |
| element.attribute = new value | Change the attribute value of the element |
| element.style.property = new style | Change the style of the element |

| Method | Description |
| :--- | :--- |
| element.setAttribute(attribute, value) | Change the attribute value of an element |

### Adding and Deleting Elements

| Method | Description |
| :--- | :--- |
| document.createElement(element) | create HTML element |
| document.removeChild(element) | remove HTML element |
| document.appendChild(element) | add HTML element |
| document.replaceChild(new, old) | replace HTML element with new one |
| document.write(text) | write into the HTML output stream |

## Creating Function for Simple JS Selector

Part of the appeal of jQuery is the simplicity of DOM traversal. These two functions can make DOM traversal seem less messy in vanilla JS:

<img src="/images/javascript-dom-01.png" width="600px">  

*[Image Source](https://css-tricks.com/now-ever-might-not-need-jquery)*

Example:

```js
const dqs = (query) => document.querySelector(query);
const dqsa = (query) => document.querySelectorAll(query);

let myElement = dqs(".class-name");
```
