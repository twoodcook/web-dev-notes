# Light Mode vs Dark Mode

source(s): [01](https://blog.prototypr.io/dark-ui-design-a-step-by-step-guide-5a78fbbbad1e), [02](https://blog.prototypr.io/using-gray-shade-and-tint-in-ui-design-589d0e638dfd),

---

## Should I Implement a Dark Mode?

To answer this question, consider the following:

- Do I need to show users a lot of data?
- Are there many interactive elements on the screen?
- Will the UI be viewed in bright light?
- Will a dark UI affect accessibility for my users?

Answering 'yes' to any of these questions may be sign you should NOT implement a dark mode. Remember that UI is about users, and accessibility. 

## Setting Elevations

<img src="/images/ligh-dark-ui-levels.png" width="80%">

Start with three layers of depth (-1, 0, 1). You can always add more, but be careful not to overwhelm users with too many layers of depth.

## Setting White Values

<img src="/images/dark-mode-values.png" width="80%">

Setting shades of white (or luminance) -- consider what elements will receive the most attention, and set high enough contrast for those areas of text (but not too high, too high of contrast can strain your eyes). Standard minimum text-to-background contrast ratio is 15.8 to 1. This can be bent for lower priority items like footer links, etc.

## Setting States

<img src="/images/dark-ui-states.png" width="40%">

This is trickier than it looks. for each state, pick a color that has enough contrast against ANY elevation, while also matching the contrast ratio of the other states being implemented.  

Consider a [Material Design Color Palette](https://www.materialui.co/colors) for this.  

Note: There is no rule saying a UI must have 4 states. Make as many as are needed (ie. pass vs fail). 

## Notes on Contrast

<img src="/images/slack-ui-example.jpeg" width="80%">

"Good contrast in a design allows the user to focus on any line at any time, while preventing the other lines from serving as distractions." [02](https://blog.prototypr.io/using-gray-shade-and-tint-in-ui-design-589d0e638dfd)  

"Today’s designer should be willing to combine light and dark themes on the same screen, resulting in a balanced hierarchy of priorities." [02](https://blog.prototypr.io/using-gray-shade-and-tint-in-ui-design-589d0e638dfd)  


