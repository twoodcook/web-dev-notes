Source(s): [Dreamhost](https://www.dreamhost.com/wordpress/guide-to-wp-templates/),

---

# Differences Between .com and .org
[Infographic](https://ithemes.com/wp-content/uploads/2013/01/WordPress.com-vs.-WordPress.org_.png)

[Interesting](https://www.quora.com/Do-web-developers-use-Wordpress-for-websites-they-build-for-clients-If-not-what-is-usually-used)

# Developing WP Header File

[Wordpress Codex - Designing Headers](https://codex.wordpress.org/Designing_Headers)

# Conditional to Display Content on Only Certain Pages

Example: Let’s say you want to display some specific text on all pages within the category bicycling. In your page.php file, you could add the following php code:

```php
if ( is_category('bicycling') ) {
echo "Check out our bicycling products on the shop page!";
}
```
