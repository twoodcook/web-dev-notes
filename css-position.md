# The Position Property

source(s): [w3schools.com](https://www.w3schools.com/css/css_positioning.asp),

---

## Summary

There are five different position values:

`static` elements are not positioned in any special way -- the default.

`relative` elements are positioned relative to their normal position.

`fixed` elements are positioned relative to the viewport and will stay in the same place even when the page is scrolled.

`absolute` elements are positioned relative to the nearest positioned ancestor -- instead of positioned relative to the viewport like in fixed.

`sticky` elements are positioned based on the user's scroll position.

Use top, bottom, left, and right top position the elements. t, b, l, r don't work unless the `position` property is set first. t, b, l, r work differently depending on the position value that is set.

## 1. Static

Elements with the value `static` are not positioned in any special way -- they adhere to the normal flow of the page.

HTML elements use this value as default. Static elements are not affected by the top, bottom, left, and right properties.

## 2. Relative

Elements with the value `relative` are positioned relative to their normal position.

Setting the top, right, bottom, and left properties of a relatively-positioned element will cause it to be adjusted away from its normal position. Other content will not be adjusted to fit into any gap left by the element.

## 3. Fixed

Elements with the value `fixed` are positioned relative to the viewport and will stay in the same place even when the page is scrolled. The top, bottom, left, and right properties are used to position the element.

A fixed element doesn't leave a gap on the page.

## 4. Absolute

Elements with the value `absolute` are positioned relative to the nearest positioned ancestor -- instead of positioned relative to the viewport like in fixed.

However, if an absolute positioned element has no positioned ancestors it uses the document body, and moves along with page scrolling. 

**Ex.)** imagine using a container `<div>` with position set to `relative` and then placing your element with `position: absolute;` within that `<div>`. It will, essentially, treat that container `<div>` the same way that the `fixed` value treats the viewport. (Probably not a good way to think of it technically, but in a way, you're creating a mini viewport for your `absolute` element to be positioned within, the same way that a `fixed` element would be positioned within the actual viewport.)

## 5. Sticky

Elements with the value `sticky` are positioned based on the user's scroll position.

A sticky element toggles between relative and fixed, depending on the scroll position. It is positioned relative until a given offset position is met in the viewport - then it "sticks" in place (like position:fixed).

**Important:** You must specify at least one of `top`, `bottom`, `left`, or `right` in order for `sticky` positioning to work.

Note: `sticky` is not supported in IE/Edge 15 or earlier. Safari requires a `-webkit-` prefix.

**Ex.)** Using `top: 0;` the sticky element sticks to the top of the page when you reach its scroll position.

