# Bootstrap

*Source(s): [w3schools.com](https://www.w3schools.com/bootstrap/default.asp)*

- Bootstrap is a free front-end framework for faster and easier web development
- Bootstrap includes HTML and CSS based design templates for typography, forms, buttons, tables, navigation, modals, image carousels and many other, as well as optional JavaScript plugins
- Bootstrap also gives you the ability to easily create responsive designs
- Bootstrap is the most popular HTML, CSS, and JavaScript framework for developing responsive, mobile-first websites.

## Bootstrap Defaults

Paragraph:  
font-size: 14px  
line-height: 1.428  
margin-bottom: /* half of its computed line height */  

Headings:  
h1 - 36px  
h2 - 30px  
h3 - 24px  
h4 - 18px  
h5 - 14px  
h6 - 12px  

## Grid System

- The grid system allows up to 12 columns across.
- Larger columns must add up to a total of 12. (ie. 4 + 4 + 4)
- The Boostrap grid is responsive and will resize with the screen size.

The grid system has four classes:
- `xs` - less than 768px
- `sm` - equal to or greater than 768px
- `md` - equal to or greater than 992px
- `lg` - equal to or greater than 1200px

Ex.  
Three equal columns = .col-sm-4  |  .col-sm-4  |  .col-sm-4  
Two unequal columns = .col-sm-4  |  .col-sm-8 

# Components

## Small  
`<small>` creates a secondary, lighter text.

## Mark
`<mark>` is light highlight

## Code
`<code>` styles to denote a piece of code

## Keyboard
`<kbd>` styles to denote a keyboard input

## Pre
`<pre>` styles the contained text in a stylized box, with monospaced font, spaces and line breaks are preserved.

## Contextual colors
The following classes are used for contextual font colors:
- .text-muted *(muted)*
- .text-primary *(important)*
- .text-success *(success!)*
- .text-info *(informational)*
- .text-warning *(warning)*
- .text-danger *(danger!)*

## Background Colors
- .bg-primary *(important)*
- .bg-success *(success!)*
- .bg-info *(informational)*
- .bg-warning *(warning)*
- .bg-danger *(danger!)*

## Typography

| Class | Description |  
|:--- |:--- |  
| .lead | Makes a paragraph stand out |  
| .text-left | left-aligned text |  
| .text-right | right-aligned text |  
| .text-center | center-aligned text |
| .list-unstyled | removes list-style and left-margin from list items |
| .pre-scrollable | makes a `<pre>` item scrollable |

## Tables

| Class | Description |
| :--- | :--- |
| `.table` | adds basic styling to a table (light padding and only horizontal dividers). |
| `.table-striped` | table with zebra striped rows. |
| `.table-bordered` | borders on all sides of table and cells. |
| `.table-hover` | table with gray hover effect on rows. |
| `.table-condensed` | more compact table (half padding) |
| `.table-responsive` | creates a responsive table, will scroll horizontally on small devices (< 768px) |

## Images

| Class | Description |
| :--- | :--- |
| `.img-responsive` | makes image responsive by adding `display:block`, `max-width:100%`, and `height:auto` to the image. |
| `.img-rounded` | rounded corners (border-radius) |
| `.img-circle` | masks image to a circle |
| `.img-thumbnail` | creates image as thumbnail (opens to larger image?) |

## Responsive Embeds
can be used on `<iframe>, <embed>, <video>, object>` elements.

| Class | Description |
| :--- | :--- |
| `.embed-responsive-item` | ? |
| `.embed-responsive` | ? |
| `.embed-responsive-16by9` | ? |
| `.embed-responsive-4by3` | ? |

## Jumbotron
`.jumbotron` inside the `.container` will make a large gray box with rounded corners.  
`.jumbotron` outside the `.container` will be full page width.
`.page-header` in a `<div>` will create a horizontal line on the bottom of the `<div>`

## Wells
`.well` will add a rounded border with gray background color and some padding.  
You can also use `.well .well-sm` and `.well .well-lg` to make a more/or less compact well (padding decrease or increase).

## Alerts
`<div class="alert alert-success">` will create a "success" alert.  
`<div class="alert alert-info">` will create a neutral informative alert.  
`<div class="alert alert-warning">` will create a warning that may need attention.  
`<div class="alert alert-danger">` will create a danger alert, indicating dangerous or potentially negative action.  

### Alert Links
Use `.alert-link` class in any `<a>` tags within an alert to create a link that matches the color of the alert it is within.

### Dismissible Alerts
Add `.alert-dismissible` to the alert container. Then add `class="close"` and `data-dismiss="alert"` to a link or a button element.  

Ex.  
```html
<div class="alert alert-success alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Indicates a successful or positive action.
</div>
```

*Note on aria and \&times;* -- To help improve accessibility for people using screen readers, you should include the aria-label="close" attribute, when creating a close button. &times; (×) is an HTML entity that is the preferred icon for close buttons, rather than the letter "x".










