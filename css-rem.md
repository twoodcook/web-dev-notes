# CSS rem Units

source(s): [sitepoint.com](https://www.sitepoint.com/understanding-and-using-rem-units-in-css/),

## rem Defined

Stands for "root em"

"Equal to the computed value of font-size on the root element. When specified on the font-size property of the root element, the rem units refer to the property’s initial value."

Basically, `1rem` is equal to the font size of the `html` element (which for most browsers defaults to 16px)

## rem Units vs. em Units

`1em` is equal to the font size of its own element.  
`1rem` is equal to the font size of the `html` element.  

### Example

Say you have the styling of your `ul` set this way...

```css
ul {
  font-size: 0.75em;
}
```

Then consider you have a child `ul` nested within the parent `ul` -- the nested list will have a font size that is 75% the size of the parent `ul` -- meaning that the two lists will not match in style.

With rem units things are simpler...

```css
html {
  font-size: 100%;
}

ul {
  font-size: 0.75rem;
}
```

This way all font sizes in the various `ul` will reference the root font size, which will keep things consistent.

## The Complexity of rem units

Some people take issue with the complexity of common rem values when you compare them to their likely pixel equivolents:

- 10px = 0.625rem
- 12px = 0.75rem
- 14px = 0.875rem
- 16px = 1rem (base)
- 18px = 1.125rem
- 20px = 1.25rem
- 24px = 1.5rem
- 30px = 1.875rem
- 32px = 2rem

## The 62.5% Rule

Another option is to use this rule to make the rem values less complex to work with. However, this introduces another challenge, which is that this forces the developer to explicitly state all font sizes for all elements (if not stated, they default to 10px, which is usually too small.)

### How it works

```css
html { font-size: 62.5%; } /* = 10px */
body { font-size: 1.4rem } /* = 14px */
h1 { font-size: 2.4rem } /* = 24px */
```

*Consider: do all browsers support rem? if not, one would need to provide an alternative to these rules. ex.* `{font-size: 24px; font-size: 2.4rem;}`
