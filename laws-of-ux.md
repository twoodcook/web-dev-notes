# Laws of UX
source(s): [www.lawsofux.com](https://lawsofux.com),

## 01 - Aesthetic Usability Effect
Users often perceive aesthetically pleasing design as design that’s more usable.

## 02 - Doherty Threshold
Productivity soars when a computer and its users interact at a pace (<400ms) that ensures that neither has to wait on the other.

## 03 - Fitts's Law
The time to acquire a target is a function of the distance to and size of the target.

## 04 - Hick's Law 
The time it takes to make a decision increases with the number and complexity of choices.

## 05 - Jakob's Law 
Users spend most of their time on other sites. This means that users prefer your site to work the same way as all the other sites they already know.

## 06 - Law of Common Region
Elements tend to be perceived into groups if they are sharing an area with a clearly defined boundary.

## 07 - Law of Pragnanz
People will perceive and interpret ambiguous or complex images as the simplest form possible, because it is the interpretation that requires the least cognitive effort of us.

## 08 - Law of Proximity
Objects that are near, or proximate to each other, tend to be grouped together.

## 09 - Law of Similarity
The human eye tends to perceive similar elements in a design as a complete picture, shape, or group, even if those elements are separated.

## 10 - Law of Uniform Connectedness
Elements that are visually connected are perceived as more related than elements with no connection.

## 11 - Miller's Law 
The average person can only keep 7 (plus or minus 2) items in their working memory.

## 12 - Occam's Razor
Among competing hypotheses that predict equally well, the one with the fewest assumptions should be selected.

## 13 - Pareto Principle
The Pareto principle states that, for many events, roughly 80% of the effects come from 20% of the causes.

## 14 - Parkinson's Law 
Any task will inflate until all of the available time is spent.

## 15 - Peak-End Rule
People judge an experience largely based on how they felt at its peak and at its end, rather than the total sum or average of every moment of the experience.

## 16 - Postel's Law
Be liberal in what you accept, and conservative in what you send.

## 17 - Serial Position Effect 
Users have a propensity to best remember the first and last items in a series.

## 18 - Tesler's Law 
Tesler's Law, also known as The Law of Conservation of Complexity, states that for any system there is a certain amount of complexity which cannot be reduced.

## 19 - Von Restorff Effect 
The Von Restorff effect, also known as The Isolation Effect, predicts that when multiple similar objects are present, the one that differs from the rest is most likely to be remembered.

## 20 - Zeigarnik Effect 
People remember uncompleted or interrupted tasks better than completed tasks.
